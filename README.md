# Demo

# links

quarkus: http://localhost:8081/hello

node: http://localhost:8082/rolldice

# port-forward

query-http -> 8080

otlp-grpc -> 4317

otlp-http -> 4318

# apps

quarkus:

```shell script
mvn quarkus:dev
```

node:

```shell script
cd src/main/node
node --require ./instrumentation.js app.js
```

