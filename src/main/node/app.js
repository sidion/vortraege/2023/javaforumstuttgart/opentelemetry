/*app.js*/
const express = require("express");
const opentelemetry = require("@opentelemetry/api");
const {} = require("express");
const {SpanKind} = require("@opentelemetry/api");

const PORT = parseInt(process.env.PORT || "8082");
const app = express();

const Client = require('node-rest-client').Client;

const client = new Client();

const myMeter = opentelemetry.metrics.getMeter('my-service-meter');
const counter = myMeter.createCounter('dice.counter');

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

app.get("/rolldice", (req, res) => {
    const tracer = opentelemetry.trace.getTracer('my-service-tracer');

    tracer.startActiveSpan("rolling", {kind: SpanKind.INTERNAL}, span => {
        client.get("http://localhost:8081/hello", (data, response) => {

            const greeting = data.toString();
            const dice = getRandomNumber(1, 6);
            res.send(`${greeting}, du hast ${dice.toString()} gewürfelt!`);
            span.addEvent("rolled", {"greeting": greeting, "dice": dice})

            span.end();
            counter.add(1, {"my-attr": "demo"});
        });
    });
});

app.listen(PORT, () => {
    console.log(`Listening for requests on http://localhost:${PORT}/rolldice`);
});