# K8s - Jaeger all in one

## create namespace

```shell script
kubectl create namespace sidion-opentelemetry
```

## apply

```shell script
kubectl apply -f jaegerallinone.yaml -n sidion-opentelemetry
```