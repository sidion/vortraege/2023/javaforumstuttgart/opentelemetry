package de.sidion;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/name")
public class NameResource {

    @Inject
    NameService nameService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @WithSpan(value = "name")
    public String name() {
        return nameService.getName();
    }
}
