package de.sidion;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    @Inject
    NameService nameService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @WithSpan
    public String hello() {
        return "Hallo " + nameService.getName();
    }
}
