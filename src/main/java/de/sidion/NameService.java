package de.sidion;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.api.trace.StatusCode;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class NameService {

    public static final String NAME = "Winfried";
    @Inject
    Tracer tracer;

    @WithSpan(value = "getName", kind = SpanKind.INTERNAL)
    public String getName() {
        var span = tracer.spanBuilder("custom span")
                .setAttribute("classname", this.getClass().getCanonicalName()).setSpanKind(SpanKind.INTERNAL).startSpan();

        span.addEvent("retrieve name", Attributes.of(AttributeKey.stringKey("name"), NAME));

        span.recordException(new IllegalStateException("nope"));
        span.setStatus(StatusCode.ERROR, "error!");

        span.end();
        return NAME;
    }

}
